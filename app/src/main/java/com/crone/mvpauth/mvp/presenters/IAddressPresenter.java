package com.crone.mvpauth.mvp.presenters;

/**
 * Created by CRN_soft on 13.12.2016.
 */

public interface IAddressPresenter {
    void clickOnAddAddress();
}
