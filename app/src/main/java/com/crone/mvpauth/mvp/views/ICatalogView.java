package com.crone.mvpauth.mvp.views;

import com.crone.mvpauth.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public interface ICatalogView extends IView {

    void showCatalogView(List<ProductDto> productsList);
    void showCurrentPage(int pageNumber);
    void updateProductCounter();

}
