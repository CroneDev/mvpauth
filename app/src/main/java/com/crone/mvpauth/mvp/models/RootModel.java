package com.crone.mvpauth.mvp.models;

/**
 * Created by CRN_soft on 12.11.2016.
 */
public class RootModel  extends AbstractModel{

    public int getItemsOnCartIcon() {
        return mDataManager.getItemsOnCartIcon();
    }

    public void setItemsOnCartIcon(int count){
        mDataManager.setItemsOnCartIcon(count);
    }

}
