package com.crone.mvpauth.mvp.views;

import com.crone.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public interface IProductView extends IView {

    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);

}
