package com.crone.mvpauth.mvp.models;

import com.crone.mvpauth.di.scopes.RootScope;
import com.crone.mvpauth.mvp.presenters.RootPresenter;

import dagger.Provides;

/**
 * Created by CRN_soft on 04.12.2016.
 */

@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
