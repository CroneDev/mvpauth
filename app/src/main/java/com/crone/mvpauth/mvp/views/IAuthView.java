package com.crone.mvpauth.mvp.views;

import android.support.annotation.Nullable;
import com.crone.mvpauth.mvp.presenters.IAuthPresenter;

/**
 * Created by CRN_soft on 21.10.2016.
 */

public interface IAuthView extends IView {

    void showLoginBtn();
    void hideLoginBtn();

    void showErrorPassword(int code);
    void hideErrorPassword();

    void showErrorEmail();
    void hideErrorEmail();

    void showCatalogScreen();


    String getUserEmail();
    String getUserPassword();

    boolean isIdle();
    void setCustomState(int state);

}
