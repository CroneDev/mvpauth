package com.crone.mvpauth.mvp.models;

import android.os.Handler;

import com.crone.mvpauth.di.DaggerService;

import com.crone.mvpauth.di.scopes.AuthScope;
import com.crone.mvpauth.ui.screens.auth.AuthScreen;


import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;


/**
 * Created by CRN_soft on 21.10.2016.
 */

public class AuthModel extends AbstractModel implements IAuthModel {

    @Override
    public boolean isAuthUser() {
        return mDataManager.getPreferencesManager().getAuthToken() != null;
    }

    private void saveAuthToken(String authToken) {
        mDataManager.getPreferencesManager().saveAuthToken(authToken);
    }

    @Override
    public void loginUser(String email, String password) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //saveAuthToken("token");
            }
        }, 4000);
    }




}
