package com.crone.mvpauth.mvp.models;

import android.net.Uri;

import com.crone.mvpauth.data.PreferencesManager;
import com.crone.mvpauth.data.storage.dto.UserAddressDto;
import com.crone.mvpauth.data.storage.dto.UserDto;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by CRN_soft on 06.12.2016.
 */

public class AccountModel extends AbstractModel {

    public UserDto getUserDtro(){
        return new UserDto(getUserProfileInfo(),getUserAddress(),getUserSettings());
    }

    private Map<String,String> getUserProfileInfo(){
        return mDataManager.getUserProfileInfo();
    }

    public ArrayList<UserAddressDto> getUserAddress(){
        return mDataManager.getUserAddress();
    }

    private Map<String,Boolean> getUserSettings(){
        return mDataManager.getUserSettings();
    }

    public void saveProfileInfo(String name,String phone){
        mDataManager.saveProfileInfo(name,phone);
    }

    public void saveAvatarPhoto(Uri photoUri){
        // TODO: 12.12.2016 Implement this
    }

    public void savePromoNotification(boolean isChecked){
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_PROMO_KEY,isChecked);
    }
    public void saveOrderNotification(boolean isChecked){
        mDataManager.saveSetting(PreferencesManager.NOTIFICATION_ORDER_KEY,isChecked);
    }

    public void addAddress(UserAddressDto userAddressDto){
        mDataManager.addAddress(userAddressDto);
    }

    public void removeAddress(int key){
        mDataManager.removeAddress(key);
    }

}
