package com.crone.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.crone.mvpauth.mvp.views.IView;


/**
 * Created by CRN_soft on 01.11.2016.
 */

public abstract class AbstractPresenter<T extends IView> {
    private T mView;

    public void takeView(T view) {
        mView = view;
    }

    public void dropView() {
        mView = null;
    }

    public abstract void initView();

    @Nullable
    public T getView() {
        return mView;
    }

}

