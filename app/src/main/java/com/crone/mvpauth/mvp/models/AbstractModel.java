package com.crone.mvpauth.mvp.models;

import com.crone.mvpauth.data.DataManager;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.components.DaggerModelComponent;
import com.crone.mvpauth.di.components.ModelComponent;
import com.crone.mvpauth.di.modules.ModelModule;

import javax.inject.Inject;

/**
 * Created by CRN_soft on 12.11.2016.
 */

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if(component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent(){
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
