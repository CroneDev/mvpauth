package com.crone.mvpauth.mvp.presenters;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public interface IProductPresenter {

    void clickOnMinus();
    void clickOnPlus();

}
