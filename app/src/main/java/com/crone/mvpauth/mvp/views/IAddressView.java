package com.crone.mvpauth.mvp.views;

import com.crone.mvpauth.data.storage.dto.UserAddressDto;

/**
 * Created by CRN_soft on 13.12.2016.
 */

public interface IAddressView extends IView{
    boolean showInputError();
    UserAddressDto getUserAddress();
}
