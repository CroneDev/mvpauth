package com.crone.mvpauth.mvp.models;

import com.crone.mvpauth.data.DataManager;
import com.crone.mvpauth.data.storage.dto.ProductDto;

import java.util.List;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public class CatalogModel extends AbstractModel {


    public CatalogModel() {

    }

    public List<ProductDto> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthToken();
    }

    public void saveCurrentPage(int pageNumber){
        mDataManager.saveCurrentPage(pageNumber);
    }

    public int getCurrentPage(){
        return mDataManager.getCurrentPage();
    }

    public ProductDto getProductById(int productId){
        // TODO: 01.11.2016  get product from datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }


}
