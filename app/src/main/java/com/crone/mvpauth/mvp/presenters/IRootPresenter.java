package com.crone.mvpauth.mvp.presenters;

/**
 * Created by CRN_soft on 13.11.2016.
 */

public interface IRootPresenter {
    void updateProductCounter(int count);
    int getProductCounter();
}
