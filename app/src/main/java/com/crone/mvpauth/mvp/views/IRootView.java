package com.crone.mvpauth.mvp.views;

import android.support.annotation.Nullable;

/**
 * Created by CRN_soft on 13.11.2016.
 */

public interface IRootView extends IView {

    void updateProductCounter(int count);
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    @Nullable
    IView getCurrentScreen();

}
