package com.crone.mvpauth.mvp.presenters;

import com.crone.mvpauth.data.storage.dto.UserAddressDto;

import java.util.ArrayList;

/**
 * Created by CRN_soft on 06.12.2016.
 */

public interface IAccountPresenter {

    void clickOnAddress();
    void switchViewState();
    void switchOrder(boolean isChecked);
    void switchPromo(boolean isChecked);

    ArrayList<UserAddressDto> getAddress();

    void takePhoto();
    void chooseCamera();
    void chooseGallery();
}
