package com.crone.mvpauth.mvp.views;

/**
 * Created by CRN_soft on 06.12.2016.
 */

public interface IAccountView extends IView{

    void changeState();

    void showEditState();

    void showPhotoSourceDialog();

    void showPreviewState();

    String getUserName();

    String getUserPhone();

}
