package com.crone.mvpauth.mvp.presenters;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public interface ICatalogPresenter {

    void clickOnBuyButton(int position);
    void showSavedPage();
    void savePage(int pageNumber);
    boolean checkUserAuth();

}
