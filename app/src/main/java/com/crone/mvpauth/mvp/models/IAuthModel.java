package com.crone.mvpauth.mvp.models;

/**
 * Created by CRN_soft on 23.10.2016.
 */

public interface IAuthModel {

    boolean isAuthUser();
    void loginUser (String email, String password);

}
