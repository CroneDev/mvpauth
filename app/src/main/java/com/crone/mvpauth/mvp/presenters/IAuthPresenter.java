package com.crone.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.crone.mvpauth.mvp.views.IAuthView;

/**
 * Created by CRN_soft on 21.10.2016.
 */

public interface IAuthPresenter {



    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();


    void onLoginSuccess();
    void onLoginError(String message);
}
