package com.crone.mvpauth.mvp.presenters;

import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.RootScope;
import com.crone.mvpauth.mvp.models.RootModel;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.mvp.views.IView;

import javax.inject.Inject;

import dagger.Provides;

/**
 * Created by CRN_soft on 12.11.2016.
 */

public class RootPresenter extends AbstractPresenter<IRootView> implements IRootPresenter{

    @Inject
    RootModel mModel;

    public RootPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        // TODO: 12.11.16 init drawer avatar + username
        if (getView() != null) {
            getView().updateProductCounter(mModel.getItemsOnCartIcon());
        }
    }


    @Override
    public void updateProductCounter(int count) {
        mModel.setItemsOnCartIcon(count);
        if(getView()!=null) {
            getView().updateProductCounter(count);
        }
    }

    @Override
    public int getProductCounter(){
        return mModel.getItemsOnCartIcon();
    }


    //region ========================= DI ======================

    private Component createDaggerComponent() {
        return DaggerRootPresenter_Component.builder()
                .module(new Module())
                .build();
    }



    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootModel provideProductModel() {
            return new RootModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @RootScope
    interface Component {
        void inject(RootPresenter presenter);
    }


    //endregion

}
