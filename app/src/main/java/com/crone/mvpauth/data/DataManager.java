package com.crone.mvpauth.data;

import com.crone.mvpauth.MyApp;
import com.crone.mvpauth.data.network.RestService;
import com.crone.mvpauth.data.storage.dto.ProductDto;
import com.crone.mvpauth.data.storage.dto.UserAddressDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.components.DaggerDataManagerComponent;
import com.crone.mvpauth.di.components.DataManagerComponent;
import com.crone.mvpauth.di.modules.LocalModule;
import com.crone.mvpauth.di.modules.NetworkModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by CRN_soft on 23.10.2016.
 */

public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;


    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if(component == null){
           component = DaggerDataManagerComponent.builder()
                   .appComponent(MyApp.getAppComponent())
                   .localModule(new LocalModule())
                   .networkModule(new NetworkModule())
                   .build();
            DaggerService.registerComponent(DataManagerComponent.class,component);
        }
        component.inject(this);
        generateMockData();
    }



    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public ProductDto getProductById(int productId) {
        // TODO: 1.11.16 this temp sample mock data fix me(may be load from db)
        return mMockProductList.get(productId - 1);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 1.11.16 update product count or status (somethink in product) save in DB
    }

    public List<ProductDto> getProductList() {
        // TODO: 1.11.16 load product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "A4 Bloody G300", "http://www.staten.ru/upload/iblock/0f2/7063d95b-f589-11e5-9220-005056a61dc5-750831.jpg", "Наушники с микрофоном A4 Bloody G300, мониторы, черный / красный", 2100, 1));
        mMockProductList.add(new ProductDto(2, "GIGABYTE GeForce GTX 1060", "http://www.3dnews.ru/assets/external/illustrations/2016/07/20/936464/754-2.jpg", "Видеокарта GIGABYTE GeForce GTX 1060, GV-N1060G1 GAMING-6GD, 6Гб, GDDR5, OC, Ret", 22000, 1));
        mMockProductList.add(new ProductDto(3, "SAMSUNG Galaxy J1 (2016)", "http://items.s1.citilink.ru/356567_v01_b.jpg", "ОС Android 5.1, экран: 4.5\", Super AMOLED, 800×480, процессор: Samsung Exynos 3475, 1300МГц, 4-х ядерный, камера: 5Мп, FM-радио, GPS, ГЛОНАСС, время работы в режиме разговора, до: 12ч", 8300, 1));
        mMockProductList.add(new ProductDto(4, "PANASONIC ES-RF31", "http://img.mvideo.ru/Pdb/20023055b.jpg", "сетчатая, количество бритвенных головок 4шт, влажное бритье, триммер, питание от аккумулятора, цвет серебристый и черный", 2400, 1));
        mMockProductList.add(new ProductDto(5, "PALIT PA-GTX1070 JETSTREAM 8G", "http://www.palit.biz/product/vga/picture/p02629/p02629_propng_24857577a0202562.png", "nVidia GeForce GTX 1070; частота процессора: 1506 МГц (1683 МГц, в режиме Boost); частота памяти: 8000МГц; объём видеопамяти: 8Гб; тип видеопамяти: GDDR5; поддержка: SLI; DirectX 12/OpenGL 4.5; доп. питание: 8 pin; блок питания не менее: 500Вт; тип поставки: Ret", 22500, 1));
        mMockProductList.add(new ProductDto(6, "Монитор ЖК LG 20MP48A-P 19.5", "http://items.s1.citilink.ru/358816_v01_b.jpg", "размер экрана: 19.5, широкоформатная матрица IPS с разрешением 1440×900, отношением сторон 16:10, яркостью 200кд/м2, временем отклика 5мс, разъем D-SUB (VGA)", 6500, 1));
        mMockProductList.add(new ProductDto(7, "Накопитель SSD KINGSTON HyperX Savage SHSS37A/240G 240Гб, 2.5", "http://user-items.s1.citilink.ru/295003_01_b.jpg", "скорость чтения, до: 520Мб/с; скорость записи, до: 510Мб/с; толщина — 7мм; адаптер 2.5 — 3.5 в комплекте; интерфейс: SATA III; форм-фактор: 2.5; TBW: 306 ТБ", 6500, 1));
        mMockProductList.add(new ProductDto(8, "Процессор INTEL Core i5 6400, LGA 1151", "http://static.nix.ru/autocatalog/intel/209544_2254_draft_large.jpg", "сокет LGA 1151, ядро Skylake, ядер — 4, потоков — 4, L3 кэш 6Мб, техпроцесс 14нм, частота 2.7 ГГц и 3.3 ГГц в режиме Turbo, поддержка памяти DDR4/ DDR3L до 64 ГБ, каналов памяти — 2, контроллер PCI Express 3.0, графическое ядро Intel HD Graphics 530, WiDi, поставка OEM", 13200, 1));
        mMockProductList.add(new ProductDto(9, "Web-камера A4 PK-760E", "http://www.a4tech.ru/images/products/repository/48UVQ-3B1B7-A4EYHM-282G.jpg", "Не требующая абсолютно никаких усилий и временных затрат в настройке web-камера A4 PK-760E станет прекрасным аксессуаром к вашему компьютеру. Для того, чтобы устройство заработало, нужно только подключить его в разъем USB, установка ПО при этом не требуется.", 900, 1));
        mMockProductList.add(new ProductDto(10, "Колонки SVEN 316", "https://mdata.yandex.net/i?path=b0925213537_img_id7156713716927239449.jpg&size=9", "акустический тип: 2.0; выходная мощность (RMS) 4Вт; регуляторы на передней панели; питание от USB; тип акустики: портативная; цвет: черный", 960, 1));
    }

    public boolean isAuthToken() {
        return getPreferencesManager().getAuthToken() != null;
    }

    public int getCurrentPage(){
        return getPreferencesManager().getCurrentPage();
    }

    public void saveCurrentPage(int pageNumber){
        getPreferencesManager().saveCurrentPage(pageNumber);
    }

    public int getItemsOnCartIcon() {
        return  getPreferencesManager().getItemsOnCart();
    }

    public void setItemsOnCartIcon(int itemsOnCartIcon) {
        getPreferencesManager().saveItemsOnCartIcon(itemsOnCartIcon);
    }

    public Map<String,String> getUserProfileInfo() {
        Map<String,String> d = new HashMap<>();
        d.put(PreferencesManager.PROFILE_FULL_NAME_KEY,"Milikhin Alexander");
        d.put(PreferencesManager.PROFILE_PHONE_KEY,"+7 (960) 837 98 95");
        d.put(PreferencesManager.PROFILE_AVATAR_KEY,"https://pp.vk.me/c637418/v637418232/1cd11/wv1cmCXEOBk.jpg");
        return d;
    }

    public ArrayList<UserAddressDto> getUserAddress() {
        UserAddressDto d = new UserAddressDto(0,"TLT","Zheleznodorojnaya","49","a",2,"LOL",true);
        UserAddressDto d2 = new UserAddressDto(1,"TLT","Zheleznodorojnaya","44","a",2,"LOL",false);
        UserAddressDto d3 = new UserAddressDto(2,"TLT","Zheleznodorojnaya","42","a",2,"LOL",false);
        UserAddressDto d4 = new UserAddressDto(3,"TLT","Zheleznodorojnaya","41","a",2,"LOL",false);
        ArrayList<UserAddressDto> c = new ArrayList<>();
        c.add(d);
        c.add(d2);
        c.add(d3);
        c.add(d4);
        return c;
    }

    public Map<String,Boolean> getUserSettings() {
        Map<String,Boolean> d = new HashMap<>();
        d.put(PreferencesManager.NOTIFICATION_ORDER_KEY,true);
        d.put(PreferencesManager.NOTIFICATION_PROMO_KEY,false);
        return d;
    }

    public void saveProfileInfo(String name, String phone) {

    }

    public void saveSetting(String notificationPromoKey, boolean isChecked) {

    }

    public void addAddress(UserAddressDto userAddressDto) {

    }

    public void removeAddress(int key) {

    }
}
