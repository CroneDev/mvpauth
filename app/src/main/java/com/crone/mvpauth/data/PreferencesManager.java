package com.crone.mvpauth.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.crone.mvpauth.utils.AppConfig;

/**
 * Created by CRN_soft on 23.10.2016.
 */

public class PreferencesManager {

    private static final String TOKEN_AUTH = "auth_token";
    private static final String PAGE_NUMBER = "page_number";
    private static final String CART_ICON_COUNT = "cart_icon_count";

    public static final String PROFILE_FULL_NAME_KEY = "profile_full_name";
    public static final String PROFILE_AVATAR_KEY = "profile_avatar";
    public static final String PROFILE_PHONE_KEY = "profile_phone";
    public static final String NOTIFICATION_ORDER_KEY = "notification_order_key";
    public static final String NOTIFICATION_PROMO_KEY = "notification_promo_key";

    private SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TOKEN_AUTH, authToken);
        editor.apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(TOKEN_AUTH, null);
    }

    public void saveCurrentPage(int pageNumber) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(PAGE_NUMBER, pageNumber);
        editor.apply();
    }

    public int getCurrentPage() {
        return mSharedPreferences.getInt(PAGE_NUMBER, 0);
    }

    public void saveItemsOnCartIcon(int itemsOnCartIcon) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(CART_ICON_COUNT,itemsOnCartIcon);
        editor.apply();
    }

    public int getItemsOnCart() {
        return mSharedPreferences.getInt(CART_ICON_COUNT, 0);
    }


}