package com.crone.mvpauth;

import android.app.Application;

import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.components.AppComponent;
import com.crone.mvpauth.di.components.DaggerAppComponent;
import com.crone.mvpauth.di.modules.AppModule;
import com.crone.mvpauth.di.modules.PicassoCacheModule;
import com.crone.mvpauth.mortar.ScreenScoper;
import com.crone.mvpauth.mvp.models.RootModule;

import com.crone.mvpauth.ui.activities.DaggerRootActivity_RootComponent;
import com.crone.mvpauth.ui.activities.RootActivity;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * Created by CRN_soft on 21.10.2016.
 */

public class MyApp extends Application {

    private static AppComponent sAppComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    private MortarScope mRootActivityScope;
    private MortarScope mRootScope;
    private RootActivity.RootComponent mRootActivityComponent;

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME,sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);

    }

    private void createAppComponent() {
        sAppComponent = DaggerAppComponent.builder().appModule(new AppModule(getApplicationContext())).build();
    }

    private void createRootActivityComponent(){
       mRootActivityComponent = DaggerRootActivity_RootComponent.builder()
               .appComponent(sAppComponent)
               .rootModule(new RootModule())
               .picassoCacheModule(new PicassoCacheModule())
               .build();
    }

}
