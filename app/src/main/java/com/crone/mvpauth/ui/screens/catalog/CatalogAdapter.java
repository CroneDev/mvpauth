package com.crone.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.ProductDto;

import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;

/**
 * Created by CRN_soft on 01.11.2016.
 */

public class CatalogAdapter extends PagerAdapter {

    private List<ProductDto> mProductList = new ArrayList<>();

   /* CatalogAdapter(List<ProductDto> productDto){
        mProductList = productDto;
    }
*/

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public void addItem(ProductDto product){
        mProductList.add(product);
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ProductDto product = mProductList.get(position);
        Context productContext = CatalogScreen.Factory.createProductContext(product,container.getContext());
        View newView = LayoutInflater.from(productContext).inflate(R.layout.screen_product,container,false);
        container.addView(newView);
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MortarScope screenScope = MortarScope.getScope(((View)object).getContext());
        container.removeView((View)object);
        screenScope.destroy();

    }
}