package com.crone.mvpauth.ui.screens.product;

import android.os.Bundle;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.ProductDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.ProductScope;
import com.crone.mvpauth.flow.AbstractScreen;
import com.crone.mvpauth.flow.Screen;
import com.crone.mvpauth.mvp.models.CatalogModel;

import com.crone.mvpauth.mvp.presenters.IProductPresenter;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.ui.screens.catalog.CatalogScreen;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by CRN_soft on 06.12.2016.
 */
@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.Component> {


    private ProductDto mProductDto;

    public ProductScreen(ProductDto productDto) {
        mProductDto = productDto;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductDto.equals(((ProductScreen) o).mProductDto);
    }

    @Override
    public int hashCode() {
        return mProductDto.hashCode();
    }

    @Override
    public Object createScreenComponent(CatalogScreen.Component parentComponent) {
        return DaggerProductScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region ========================= DI =========================
    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter preseter);

        void inject(ProductView productView);
    }
    //endregion

    //region ========================= Presenter =========================
    public class ProductPresenter extends ViewPresenter<ProductView> implements IProductPresenter {

        @Inject
        CatalogModel mCatalogModel;

        private ProductDto mProductDto;

        ProductPresenter(ProductDto productDto) {
            mProductDto = productDto;
        }


        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showProductView(mProductDto);
            }
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void clickOnMinus() {
            if (mProductDto.getCount() > 1) {
                mProductDto.deleteProduct();
                mCatalogModel.updateProduct(mProductDto);
                if (getView() != null) {
                    getView().updateProductCountView(mProductDto);
                }
            }
        }

        @Override
        public void clickOnPlus() {
            mProductDto.addProduct();
            mCatalogModel.updateProduct(mProductDto);
            if (getView() != null) {
                getView().updateProductCountView(mProductDto);
            }
        }
    }
    //endregion
}
