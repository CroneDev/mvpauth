package com.crone.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.ProductDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.AuthScope;
import com.crone.mvpauth.di.scopes.CatalogScope;
import com.crone.mvpauth.flow.AbstractScreen;
import com.crone.mvpauth.flow.Screen;
import com.crone.mvpauth.mvp.models.AuthModel;
import com.crone.mvpauth.mvp.models.CatalogModel;
import com.crone.mvpauth.mvp.presenters.ICatalogPresenter;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.ui.activities.RootActivity;
import com.crone.mvpauth.ui.screens.auth.AuthScreen;
import com.crone.mvpauth.ui.screens.product.ProductScreen;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by CRN_soft on 06.12.2016.
 */
@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }


    //region ========================= DI =========================
    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter catalogPresenter);

        void inject(CatalogView catalogView);

        CatalogModel getCatalogModel();

        Picasso getPicasso();
    }

    //endregion

    //region ========================= Presenter =========================
    public class CatalogPresenter extends ViewPresenter<CatalogView> implements ICatalogPresenter {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        CatalogModel mCatalogModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }


        @Override
        protected void onLoad(Bundle savedInstanceState) {
            if (getView() != null) {
                getView().showCatalogView(mCatalogModel.getProductList());
            }
        }

        @Override
        public void clickOnBuyButton(int position) {
            if (getView() != null && getRootView() != null) {
                if (checkUserAuth()) {
                    getRootView().showMessage("Товар " + mCatalogModel.getProductList().get(position).getProductName() + " успешно добавлен корзину");
                    mRootPresenter.updateProductCounter(mRootPresenter.getProductCounter() + 1);
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }
            }
        }

        @Override
        public void showSavedPage() {
            if (getView() != null) {
                getView().showCurrentPage(mCatalogModel.getCurrentPage());
            }
        }

        @Override
        public void savePage(int pageNumber) {
            mCatalogModel.saveCurrentPage(pageNumber);
        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAuth();
        }
    }
    //endregion

    public static class Factory {
        static Context createProductContext(ProductDto productDto, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope = null;
            ProductScreen screen = new ProductScreen(productDto);
            String scopeName = String.format(Locale.ENGLISH, "%s_%d", screen.getScopeName(), productDto.getId());
            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME, screen.createScreenComponent(DaggerService.<CatalogScreen.Component>getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }

}
