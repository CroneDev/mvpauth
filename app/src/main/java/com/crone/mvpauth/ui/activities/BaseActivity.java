package com.crone.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.crone.mvpauth.R;
import com.crone.mvpauth.flow.Screen;

/**
 * Created by CRN_soft on 04.12.2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected ProgressDialog mProgressDialog;
    private static final String IS_SHOWING = "is_showing";
    private boolean isShowing;

    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.load));
        }
        mProgressDialog.show();
        isShowing = true;
    }

    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        isShowing = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_SHOWING,isShowing);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState.getBoolean(IS_SHOWING)){
            showProgress();
        }
    }
}