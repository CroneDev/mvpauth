package com.crone.mvpauth.ui.screens.address;

import android.support.annotation.NonNull;

import com.crone.mvpauth.R;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.AddressScope;
import com.crone.mvpauth.flow.AbstractScreen;
import com.crone.mvpauth.flow.Screen;
import com.crone.mvpauth.mvp.models.AccountModel;
import com.crone.mvpauth.mvp.presenters.IAddressPresenter;
import com.crone.mvpauth.ui.screens.account.AccountScreen;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by CRN_soft on 13.12.2016.
 */
@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {
    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @NonNull
    @Override
    public Object getParentKey() {
        return null;
    }

    //region =============================== DI ===============================
    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView addressView);
    }
    //endregion


    //region =============================== Presenter ===============================
    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter {

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void clickOnAddAddress() {
            // TODO: 13.12.2016 Save address in model
            if (getView() != null) {
                if (!getView().showInputError()) {
                    mAccountModel.addAddress(getView().getUserAddress());
                    Flow.get(getView()).goBack();
                }
            }
        }
    }
    //endregion

}
