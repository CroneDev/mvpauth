package com.crone.mvpauth.ui.screens.account;

import android.accounts.Account;
import android.net.Uri;
import android.os.Bundle;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.UserAddressDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.AccountScope;
import com.crone.mvpauth.flow.AbstractScreen;
import com.crone.mvpauth.flow.Screen;
import com.crone.mvpauth.mvp.models.AccountModel;
import com.crone.mvpauth.mvp.presenters.AbstractPresenter;
import com.crone.mvpauth.mvp.presenters.IAccountPresenter;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.ui.activities.RootActivity;
import com.crone.mvpauth.ui.screens.address.AddressScreen;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.Presenter;
import mortar.ViewPresenter;

/**
 * Created by CRN_soft on 06.12.2016.
 */
@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ========================= DI =========================
    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountModel provideAccountModel() {
            return new AccountModel();
        }

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView accountView);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();

    }
    //endregion

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int mCustomState) {
        this.mCustomState = mCustomState;
    }

    //region =============================== Presenter ===============================
    public class AccountPresenter extends ViewPresenter<AccountView> implements IAccountPresenter {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;

        private Uri mAvatarUri;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView(mAccountModel.getUserDtro());
            }
        }

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen());
        }

        @Override
        public void switchViewState() {
            if (getView() != null) {
                if (getCustomState() == AccountView.EDIT_STATE) {
                    mAccountModel.saveProfileInfo(getView().getUserName(), getView().getUserPhone());
                    mAccountModel.saveAvatarPhoto(mAvatarUri);
                }
                getView().changeState();
            }
        }

        @Override
        public void switchOrder(boolean isChecked) {
            mAccountModel.saveOrderNotification(isChecked);
        }

        @Override
        public void switchPromo(boolean isChecked) {
            mAccountModel.savePromoNotification(isChecked);
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }

        }

        @Override
        public void chooseCamera() {
            if (getView() != null) {
                getRootView().showMessage("Choose Camera");
            }
            // TODO: 12.12.2016 choose from camera
        }

        @Override
        public void chooseGallery() {
            if (getView() != null) {
                getRootView().showMessage("Choose Gallery");
            }
            // TODO: 12.12.2016 choose from gallery
        }

        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        public ArrayList<UserAddressDto> getAddress() {
            return mAccountModel.getUserAddress();
        }
    }
    //endregion
}
