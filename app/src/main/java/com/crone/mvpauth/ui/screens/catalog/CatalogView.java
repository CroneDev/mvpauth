package com.crone.mvpauth.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.ProductDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.mvp.views.ICatalogView;
import com.crone.mvpauth.utils.AppConfig;
import com.crone.mvpauth.utils.Typefaces;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by CRN_soft on 06.12.2016.
 */

public class CatalogView extends RelativeLayout implements ICatalogView {

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;

    @BindView(R.id.add_to_card_btn)
    Button mAddToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ========================= LifeCycle =========================
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mAddToCardBtn.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_BOOK));

        }
    }

    //onFinishInflate - вызывается первее чем атач
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    //region ========================= Events =========================
    @OnClick(R.id.add_to_card_btn)
    void addToCartBtn() {
        mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
    }
    //endregion

    //region ========================= ICatalogView =========================
    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter();
         for (ProductDto product : productsList) {
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
        mProductPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPresenter.savePage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIndicator.setViewPager(mProductPager);
        mPresenter.showSavedPage();

    }

    @Override
    public void showCurrentPage(int pageNumber) {
        mProductPager.setCurrentItem(pageNumber, true);

    }

    @Override
    public void updateProductCounter() {

    }

    @Override
    public boolean onViewBackPressed() {
        return false;
    }
    //endregion
}
