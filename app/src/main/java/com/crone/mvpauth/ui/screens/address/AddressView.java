package com.crone.mvpauth.ui.screens.address;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.UserAddressDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.mvp.views.IAddressView;
import com.crone.mvpauth.utils.AppConfig;
import com.crone.mvpauth.utils.Typefaces;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by CRN_soft on 13.12.2016.
 */

public class AddressView extends RelativeLayout implements IAddressView {

    @BindView(R.id.add_button)
    Button mAddButton;
    @BindView(R.id.address_city)
    TextInputLayout mAdressCity;
    @BindView(R.id.address_number)
    TextInputLayout mAdressNumber;
    @BindView(R.id.address_home)
    TextInputLayout mAdressHome;
    @BindView(R.id.address_floor)
    TextInputLayout mAdressFloor;
    @BindView(R.id.address_street)
    TextInputLayout mAdressStreet;
    @BindView(R.id.address_comments)
    TextInputLayout mAdressComments;
    @BindView(R.id.favorite_address)
    SwitchCompat mFavoriteAddress;

    private int mAddressId = 0;

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }


    //region ========================= LifeCycle =========================
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mAddButton.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_REGULAR));
        }
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    public void initView(@Nullable UserAddressDto address) {
        if (address != null) {
            mAddressId = address.getId();
            mAdressCity.getEditText().setText(address.getName());
            mAdressStreet.getEditText().setText(address.getStreet());
            mAdressHome.getEditText().setText(address.getHouse());
            mAdressNumber.getEditText().setText(address.getApartment());
            mAdressFloor.getEditText().setText(String.valueOf(address.getFloor()));
            mAdressComments.getEditText().setText(address.getComment());
            mFavoriteAddress.setChecked(address.isFavorite());
            mAddButton.setText(R.string.address_save);
        }
    }

    //region =============================== IAddressView ===============================
    @Override
    public boolean showInputError() {
        //Обнуляем поля
        mAdressCity.setError(null);
        mAdressStreet.setError(null);
        mAdressHome.setError(null);
        mAdressNumber.setError(null);
        mAdressFloor.setError(null);

        //Поле город
        if (mAdressCity.getEditText().getText().length() == 0) {
            mAdressCity.setError(getContext().getString(R.string.input_error));
            mAdressCity.requestFocus();
            return true;
        }
        //Поле улица
        if (mAdressStreet.getEditText().getText().length() == 0) {
            mAdressStreet.setError(getContext().getString(R.string.input_error));
            mAdressStreet.requestFocus();
            return true;
        }
        //Поле номер дома
        if (mAdressHome.getEditText().getText().length() == 0) {
            mAdressHome.setError(getContext().getString(R.string.input_error));
            mAdressHome.requestFocus();
            return true;
        }
        //Поле номер квартиры
        if (mAdressNumber.getEditText().getText().length() == 0) {
            mAdressNumber.setError(getContext().getString(R.string.input_error));
            mAdressNumber.requestFocus();
            return true;
        }
        //Поле этаж
        if (mAdressFloor.getEditText().getText().length() == 0) {
            mAdressFloor.setError(getContext().getString(R.string.input_error));
            mAdressFloor.requestFocus();
            return true;
        }
        return false;
    }

    @Override
    public UserAddressDto getUserAddress() {

        return new UserAddressDto(mAddressId,
                mAdressCity.getEditText().getText().toString(),
                mAdressStreet.getEditText().getText().toString(),
                mAdressHome.getEditText().getText().toString(),
                mAdressNumber.getEditText().getText().toString(),
                Integer.valueOf(mAdressFloor.getEditText().getText().toString()),
                mAdressComments.getEditText().getText().toString(),
                mFavoriteAddress.isChecked());
    }

    @Override
    public boolean onViewBackPressed() {
        return false;
    }
    //endregion

    //region =============================== Eventes ===============================
    @OnClick(R.id.add_button)
    void addAddressBtn() {
        mPresenter.clickOnAddAddress();
    }
    //endregion
}
