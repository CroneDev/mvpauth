package com.crone.mvpauth.ui.screens.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.crone.mvpauth.R;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.mvp.views.IAuthView;

import com.crone.mvpauth.ui.activities.RootActivity;
import com.crone.mvpauth.utils.AppConfig;
import com.crone.mvpauth.utils.NetworkStatusChecker;
import com.crone.mvpauth.utils.Typefaces;

import android.util.AttributeSet;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * Created by CRN_soft on 30.11.2016.
 */

public class AuthView extends RelativeLayout implements IAuthView {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.social_vk)
    ImageButton mSocialVK;
    @BindView(R.id.social_twitter)
    ImageButton mSocialTwitter;
    @BindView(R.id.social_fb)
    ImageButton mSocialFB;
    @BindView(R.id.app_name_txt)
    TextView mMainTitle;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mTextInputLayoutPass;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mTextInputLayoutEmail;

    private AuthScreen mScreen;

    @Inject
    AuthScreen.AuthPresenter mPresenter;

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }

    }


    //region ========================= LifeCycle =========================
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mLoginBtn.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_BOOK));
            mShowCatalogBtn.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_BOOK));
            mMainTitle.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_BOOK));
        }
    }

    //onFinishInflate - вызывается первее чем атач
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        if (!isInEditMode()) {
            showViewFromState();
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    //region =============================== Custom ===============================

    private void showViewFromState() {
        if (mScreen.getCustomState() == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        //Animation only for >= 21 api
        if (mAuthCard.isAttachedToWindow() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animShowCard(mAuthCard);
        } else {
            mAuthCard.setVisibility(VISIBLE);
        }

        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        //Animation only for >= 21 api
        if (mAuthCard.isAttachedToWindow() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            animHideCard(mAuthCard);
        } else {
            mAuthCard.setVisibility(INVISIBLE);
            mShowCatalogBtn.setVisibility(VISIBLE);
        }

    }

    //endregion

    //region ========================= Events =========================

    @OnClick(R.id.login_btn)
    void loginClick() {
        if (!NetworkStatusChecker.isNetworkAvailable(getContext())) {
            mPresenter.onLoginError("Network issue");
            return;
        }
        mPresenter.clickOnLogin();
    }

    @OnClick(R.id.show_catalog_btn)
    void catalogClick() {
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.social_fb)
    void fbClick() {
        mPresenter.clickOnFb();
    }

    @OnClick(R.id.social_vk)
    void vkClick() {
        mPresenter.clickOnVk();
    }

    @OnClick(R.id.social_twitter)
    void twClick() {
        mPresenter.clickOnTwitter();
    }

    //endregion

    //region ========================= IAuthVIew =========================
    @Override
    public void showLoginBtn() {
        mSocialFB.setVisibility(VISIBLE);
        mSocialVK.setVisibility(VISIBLE);
        mSocialTwitter.setVisibility(VISIBLE);
        mLoginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mSocialFB.setVisibility(GONE);
        mSocialVK.setVisibility(GONE);
        mSocialTwitter.setVisibility(GONE);
        mLoginBtn.setVisibility(GONE);
    }

    @Override
    public void showErrorPassword(int code) {
        mTextInputLayoutPass.setFocusable(true);
        mTextInputLayoutPass.setErrorEnabled(true);
        switch (code) {
            case 0:
                mTextInputLayoutPass.setError(getContext().getString(R.string.pass_error_short));
                break;
            case 1:
                mTextInputLayoutPass.setError(getContext().getString(R.string.pass_error_space));
                break;
        }
        mTextInputLayoutPass.requestFocus();
    }

    @Override
    public void hideErrorPassword() {
        mTextInputLayoutPass.setError(null);
        mTextInputLayoutPass.setErrorEnabled(false);
    }

    @Override
    public void showErrorEmail() {
        mTextInputLayoutEmail.setFocusable(true);
        mTextInputLayoutEmail.setErrorEnabled(true);
        mTextInputLayoutEmail.setError(getContext().getString(R.string.email_error));
        mTextInputLayoutEmail.requestFocus();
    }

    @Override
    public void hideErrorEmail() {
        mTextInputLayoutEmail.setError(null);
        mTextInputLayoutEmail.setErrorEnabled(false);
    }


    @Override
    public void showCatalogScreen() {
        mPresenter.clickOnShowCatalog();
    }


    @Override
    public String getUserEmail() {
        return String.valueOf(mEmailEt.getText());
    }

    @Override
    public String getUserPassword() {
        return String.valueOf(mPasswordEt.getText());
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() != LOGIN_STATE;
    }

    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }


    @Override
    public boolean onViewBackPressed() {
        if (!isIdle()) {
            setCustomState(IDLE_STATE);
            return true;
        } else
            return false;
    }
    //endregion

    //region =================== Animation expand AuthPanel ===============
    private void animShowCard(CardView myView) {
        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight());
        int cy = (myView.getTop() + myView.getBottom());

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getMeasuredWidth(), myView.getMeasuredHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        myView.setVisibility(VISIBLE);

        anim.start();
    }

    private void animHideCard(final CardView myView) {
        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = (myView.getTop() + myView.getBottom()) / 2;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth();

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(INVISIBLE);
                mShowCatalogBtn.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mShowCatalogBtn.setVisibility(VISIBLE);
                    }
                }, 100);
            }
        });

        // start the animation
        anim.start();
    }
    //endregion
}
