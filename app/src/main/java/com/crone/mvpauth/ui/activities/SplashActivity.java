package com.crone.mvpauth.ui.activities;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.crone.mvpauth.BuildConfig;
import com.crone.mvpauth.R;

import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.flow.TreeKeyDispather;
import com.crone.mvpauth.mortar.ScreenScoper;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.mvp.views.IView;

import com.crone.mvpauth.ui.screens.auth.AuthScreen;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class SplashActivity extends BaseActivity implements IRootView {

    @Inject
    RootPresenter mRootPresenter;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    //region ======================== Life cycle ========================

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispather(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        DaggerService.<RootActivity.RootComponent>getDaggerComponent(this).inject(this);




        /*animateSocial(mSocialFB);
        animateSocial(mSocialVK);
        animateSocial(mSocialTwitter);*/

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        mRootPresenter.dropView();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRootPresenter.takeView(this);
    }

    @Override
    protected void onDestroy() {

        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    //endregion


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getCurrentScreen() != null && !getCurrentScreen().onViewBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }
    }

    //region ======================== IAuthView ========================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            // TODO:   send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public boolean onViewBackPressed() {
        return false;
    }

    @Override
    public void updateProductCounter(int count) {

    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }

  /*  @Override
    public void showLoad() {
        getAuthPanel().showLoad();
    }

    @Override
    public void hideLoad() {
        getAuthPanel().hideLoad();
    }
 */
   /* @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mSocialFB.setVisibility(View.GONE);
        mSocialVK.setVisibility(View.GONE);
        mSocialTwitter.setVisibility(View.GONE);
        mLoginBtn.setVisibility(View.GONE);
    }*/


    public void startRootActivity() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivityForResult(intent, 1);
        ActivityCompat.finishAffinity(this);
    }
  /*
    @Override
    public void finishActivity() {
        ActivityCompat.finishAffinity(this);
    }
    //endregion

    @Override
    public void onBackPressed() {
        if (getAuthPanel() != null && !getAuthPanel().isIdle()) {
            if (getAuthPanel().getLoadState() == AuthPanel.SHOW_LOAD) return;
            getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.social_vk:
                mPresenter.clickOnVk();
                break;
            case R.id.social_fb:
                mPresenter.clickOnFb();
                break;
            case R.id.social_twitter:
                mPresenter.clickOnTwitter();
                break;

        }
    }

    //При нажатии кнопки "В корзину", открываем логин панель с анимацией
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        boolean auth = data.getBooleanExtra("animateAuth", false);
        if (auth) {
            if (getAuthPanel() != null) {
                //animation
                getAuthPanel().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
                    }
                }, 400);
            }

        }

    }

    private void animateSocial(ImageButton button) {
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.animate().setDuration(100).scaleX(1.15f).scaleY(1.15f);
                        break;

                    case MotionEvent.ACTION_UP:
                        v.animate().setDuration(100).scaleX(1.0f).scaleY(1.0f);
                        break;
                }
                return false;
            }
        });
    }*/


}
