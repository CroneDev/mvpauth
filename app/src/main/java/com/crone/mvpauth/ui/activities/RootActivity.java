package com.crone.mvpauth.ui.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.crone.mvpauth.BuildConfig;
import com.crone.mvpauth.R;

import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.components.AppComponent;
import com.crone.mvpauth.di.modules.PicassoCacheModule;
import com.crone.mvpauth.di.scopes.RootScope;
import com.crone.mvpauth.flow.TreeKeyDispather;
import com.crone.mvpauth.mvp.models.RootModule;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.mvp.views.IView;
import com.crone.mvpauth.ui.screens.account.AccountScreen;
import com.crone.mvpauth.ui.screens.auth.AuthScreen;
import com.crone.mvpauth.ui.screens.catalog.CatalogScreen;
import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mikepenz.actionitembadge.library.utils.BadgeStyle;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout mRootFrame;

    @Inject
    RootPresenter mRootPresenter;

    private MenuItem mCartMenu;


    //region ================== LifeCycle =======================

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new CatalogScreen())
                .dispatcher(new TreeKeyDispather(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);


        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);

    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }
    //endregion

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mNavView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Object key = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                key = new AccountScreen();
                break;
            case R.id.nav_catalog:
                key = new CatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }

        if(key!=null){
            Flow.get(this).set(key);
        }


        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_root_toolbar_menu, menu);
        mCartMenu = menu.findItem(R.id.action_cart);
        // инициализируем презентер, когда тулбар готов
        mRootPresenter.initView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_cart:
                // TODO: 12.11.2016 Click cart on toolbar
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().onViewBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        } else {
            new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.exit_dialog_title)
                    .setMessage(R.string.exit_dialog_message)
                    .setPositiveButton(R.string.exit_dialog_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }
                    }).setNegativeButton(R.string.exit_dialog_no, null).show();

        }
    }

    /**
     * Задаем выделение айтема в NavigationDrawer
     *
     * @param id - menu id
     */
    public void setCheckedItem(int id) {
        mNavView.setCheckedItem(id);
    }


    private BadgeStyle getBadgeStyle() {
        return new BadgeStyle(BadgeStyle.Style.DEFAULT, com.mikepenz.actionitembadge.library.R.layout.menu_action_item_badge, ContextCompat.getColor(this, R.color.colorAccent), ContextCompat.getColor(this, R.color.colorUnselected), Color.WHITE);
    }

    //region ================== IRootView =======================
    @Override
    public void updateProductCounter(int count) {
        if (count == 0) {
            ActionItemBadge.update(this, mCartMenu, ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart_black_24dp), getBadgeStyle(), null);
        } else {
            ActionItemBadge.update(this, mCartMenu, ContextCompat.getDrawable(this, R.drawable.ic_shopping_cart_black_24dp), getBadgeStyle(), count);

        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
            // TODO:   send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mRootFrame.getChildAt(0);
    }


    //endregion

    //region ========================= DI =========================


    @Override
    public boolean onViewBackPressed() {

        return false;
    }


    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(SplashActivity splashActivity);

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }

    //endregion


}
