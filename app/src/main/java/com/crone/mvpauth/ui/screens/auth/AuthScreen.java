package com.crone.mvpauth.ui.screens.auth;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.util.PatternsCompat;
import android.widget.Toast;

import com.crone.mvpauth.R;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.di.scopes.AuthScope;
import com.crone.mvpauth.flow.AbstractScreen;
import com.crone.mvpauth.flow.Screen;
import com.crone.mvpauth.mvp.models.AuthModel;


import com.crone.mvpauth.mvp.presenters.IAuthPresenter;
import com.crone.mvpauth.mvp.presenters.RootPresenter;
import com.crone.mvpauth.mvp.views.IRootView;
import com.crone.mvpauth.ui.activities.RootActivity;

import com.crone.mvpauth.ui.activities.SplashActivity;
import com.crone.mvpauth.utils.AppConfig;

import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

import static com.crone.mvpauth.ui.screens.auth.AuthView.LOGIN_STATE;

/**
 * Created by CRN_soft on 30.11.2016.
 */

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int state) {
        mCustomState = state;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region ========================= DI =========================
    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter providePreseter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter authPresenter);

        void inject(AuthView view);
    }
    //endregion

    //region ========================= DI =========================
    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        AuthModel mAuthModel;
        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }


        //region ===================== Validate Email and Pass ======================
        public boolean isValidateEmail() {
            if (!PatternsCompat.EMAIL_ADDRESS.matcher(getView().getUserEmail()).find() || Pattern.compile("\\s").matcher(getView().getUserEmail()).find()) {
                getView().showErrorEmail();
                return false;
            } else {
                getView().hideErrorEmail();
                return true;
            }
        }


        public boolean isValidatePassword() {
            //Проверяем длину символов
            if (getView().getUserPassword().trim().length() < AppConfig.MINIMUM_CHARACTERS) {
                getView().showErrorPassword(0);
                return false;
            //Проверяем пробелы
            } else if (Pattern.compile("\\s").matcher(getView().getUserPassword()).find()) {
                getView().showErrorPassword(1);
                return false;
            } else {
                getView().hideErrorPassword();
                return true;
            }
        }
        //endregion


        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                if (!checkUserAuth()) {
                    getView().showLoginBtn();
                } else {
                    getView().hideLoginBtn();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public void clickOnLogin() {
            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCustomState(LOGIN_STATE);
                } else {
                    if (!isValidateEmail()) {
                        return;
                    }
                    if (!isValidatePassword()) {
                        return;
                    }
                    //TODO: auth user
                    getRootView().showLoad();
                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                }
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView() != null) {
                getRootView().showMessage("клик фб");
            }
        }

        @Override
        public void clickOnVk() {
            if (getRootView() != null) {
                getRootView().showMessage("клик вк");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView() != null) {
                getRootView().showMessage("клик твиттер");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            if (getRootView() != null) {
                // TODO: 01.11.2016 if update data complete start catalog screen
                getRootView().showMessage("Показать каталог");
                if(getRootView() instanceof SplashActivity){
                    ((SplashActivity)getRootView()).startRootActivity();
                } else {
                    // TODO: 06.12.2016 show catalog screen
                }
            }

        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }


        @Override
        public void onLoginSuccess() {
            if (getView() != null && getRootView() != null) {
                getRootView().hideLoad();
                getView().hideLoginBtn();
                getView().setCustomState(AuthView.IDLE_STATE);
                //задержка открытия каталога
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getView().showCatalogScreen();
                        //getView().finishActivity();
                    }
                }, 1000);
            }
        }

        @Override
        public void onLoginError(String message) {
            if (getView() != null && getRootView() != null) {
                getRootView().hideLoad();
                getRootView().showMessage("Error: " + message);
            }
        }
    }
    //endregion

}
