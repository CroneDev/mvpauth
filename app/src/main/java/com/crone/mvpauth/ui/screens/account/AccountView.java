package com.crone.mvpauth.ui.screens.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;
import android.util.TypedValue;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.UserDto;
import com.crone.mvpauth.di.DaggerService;
import com.crone.mvpauth.mvp.views.IAccountView;
import com.crone.mvpauth.utils.AppConfig;
import com.crone.mvpauth.utils.Typefaces;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;

import butterknife.BindBitmap;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * Created by CRN_soft on 06.12.2016.
 */

public class AccountView extends CoordinatorLayout implements IAccountView {

    public static final int PREVIEW_STATE = 1;
    public static final int EDIT_STATE = 0;
    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;

    @BindView(R.id.profile_name_txt)
    TextView profileNameTxt;
    @BindView(R.id.user_avatar_img)
    ImageView userAvatarImg;
    @BindView(R.id.user_phone_et)
    EditText userPhoneEt;
    @BindView(R.id.user_full_name_et)
    EditText userFullNameEt;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout profileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView addressList;
    @BindView(R.id.add_address_btn)
    Button addAddressBtn;
    @BindView(R.id.notification_order_sw)
    SwitchCompat notificationOrderSw;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat notificationPromoSw;

    private AccountScreen mScreen;
    private UserDto mUserDto;
    private TextWatcher mWather;

    //swipe
    private Paint p = new Paint();
    final float ALPHA_FULL = 1.0f;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ========================= LifeCycle =========================
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            addAddressBtn.setTypeface(Typefaces.get(getContext(), AppConfig.FONTS_BEBAS_NEUE_REGULAR));
        }
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }
    //endregion

    public void initView(UserDto user) {
        mUserDto = user;
        initProfileInfo();
        initList();
        initSettings();
        showViewFromState();
        setUpItemTouchHelper();
    }

    private void initSettings() {
        notificationOrderSw.setChecked(mUserDto.isOrderNotification());
        notificationOrderSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mPresenter.switchOrder(b);
            }
        });
        notificationPromoSw.setChecked(mUserDto.isPromoNotification());
        notificationPromoSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mPresenter.switchPromo(b);
            }
        });
    }

    private void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        addressList.setLayoutManager(layoutManager);
        addressList.swapAdapter(new AccountAdapter(mPresenter.getAddress()), true);
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    addressList.getAdapter().notifyItemRemoved(position);
                } else {
                    // TODO: 18.12.2016 EDIT ITEM
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.ic_mode_edit_black_24dp);
                        icon = drawableToBitmap(d);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        Drawable d = ContextCompat.getDrawable(getContext(), R.drawable.ic_delete_black_24dp);
                        icon = drawableToBitmap(d);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(addressList);
    }

    private void initProfileInfo() {
        profileNameTxt.setText(mUserDto.getFullname());
        userFullNameEt.setText(mUserDto.getFullname());
        userPhoneEt.setText(mUserDto.getPhone());
        mPicasso.load(mUserDto.getAvatar())
                .into(userAvatarImg);
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    /**
     * Превращаем VectorDrawable в битмап
     * @param drawable - VectorDrawable
     * @return bitmap
     */
    private Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 128, r.getDisplayMetrics());
        Bitmap bitmap = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    //region =============================== IAccountView ===============================
    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mScreen.setCustomState(PREVIEW_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        mWather = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                profileNameTxt.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        userFullNameEt.addTextChangedListener(mWather);
        //Задаем фокус
        userFullNameEt.requestFocus();

        profileNameWrapper.setVisibility(VISIBLE);
        userPhoneEt.setEnabled(true);

        mPicasso.load(R.drawable.ic_add_black_24dp)
                .error(R.drawable.ic_add_black_24dp)
                .into(userAvatarImg);
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {"Загрузить из галереи", "Сделать фото", "Отмена"};
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        mPresenter.chooseGallery();
                        break;
                    case 1:
                        mPresenter.chooseCamera();
                        break;
                    case 2:
                        dialogInterface.cancel();
                        break;
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void showPreviewState() {
        profileNameWrapper.setVisibility(GONE);
        userPhoneEt.setEnabled(false);

        if (mWather != null) {
            userFullNameEt.removeTextChangedListener(mWather);
        }
        mPicasso.load(mUserDto.getAvatar())
                .into(new Target() {

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                        dr.setCircular(true);
                        userAvatarImg.setImageDrawable(dr);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }

                });
    }

    @Override
    public String getUserName() {
        return String.valueOf(userFullNameEt.getText());
    }

    @Override
    public String getUserPhone() {
        return String.valueOf(userPhoneEt.getText());
    }

    @Override
    public boolean onViewBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            changeState();
            return true;
        } else {
            return false;
        }
    }
    //endregion

    //region =============================== Events ===============================
    // TODO: 13.12.2016 Swipe
    @OnClick(R.id.collapsing_toolbar_account)
    void editMode() {
        mPresenter.switchViewState();
    }

    @OnClick(R.id.add_address_btn)
    void addAddressBtn() {
        mPresenter.clickOnAddress();
    }
    //endregion
}
