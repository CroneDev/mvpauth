package com.crone.mvpauth.ui.screens.account;

import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.crone.mvpauth.R;
import com.crone.mvpauth.data.storage.dto.UserAddressDto;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by CRN_soft on 18.12.2016.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountViewHolder> {

    private ArrayList<UserAddressDto> mAddressList;

    AccountAdapter(ArrayList<UserAddressDto> userAddressDtos) {
        mAddressList = userAddressDtos;
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_item, parent, false);
        return new AccountViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final AccountViewHolder holder, int position) {
        UserAddressDto address = mAddressList.get(position);

        String tmpStr = address.getStreet() + " " + address.getHouse();

        if (!address.getApartment().isEmpty()) tmpStr = tmpStr + " - " + address.getApartment();
        if(mAddressList.get(position).isFavorite()){
            holder.icon.setImageDrawable(ContextCompat.getDrawable(holder.icon.getContext(),R.drawable.ic_crown));
        }
        holder.address.setText(tmpStr);
        if (address.getComment().isEmpty()) holder.addressComment.setVisibility(View.GONE);
        else holder.addressComment.setText(address.getComment());
    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }


    public static class AccountViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address_et)
        EditText address;

        @BindView(R.id.address_comment_et)
        EditText addressComment;

        @BindView(R.id.address_item_icon)
        ImageView icon;

        public AccountViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
