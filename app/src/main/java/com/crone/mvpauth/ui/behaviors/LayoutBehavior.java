package com.crone.mvpauth.ui.behaviors;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.crone.mvpauth.R;

/**
 * Created by CRN_soft on 12.12.2016.
 */

public class LayoutBehavior extends AppBarLayout.ScrollingViewBehavior {

    public LayoutBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
        lp.setMargins(0, dependency.getBottom() / 4, 0, 0);
        child.setLayoutParams(lp);
        return super.onDependentViewChanged(parent, child, dependency);
    }


}
