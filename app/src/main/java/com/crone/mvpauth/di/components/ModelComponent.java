package com.crone.mvpauth.di.components;

import com.crone.mvpauth.di.modules.ModelModule;
import com.crone.mvpauth.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}