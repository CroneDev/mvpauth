package com.crone.mvpauth.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by CRN_soft on 12.11.2016.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CatalogScope {
}
