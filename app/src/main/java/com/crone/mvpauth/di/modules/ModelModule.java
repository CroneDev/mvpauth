package com.crone.mvpauth.di.modules;

import com.crone.mvpauth.data.DataManager;

import javax.inject.Scope;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager(){
        return new DataManager();
    }

}
