package com.crone.mvpauth.di.modules;

import android.content.Context;

import com.crone.mvpauth.data.PreferencesManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context){
        return new PreferencesManager(context);
    }

}