package com.crone.mvpauth.di.components;

import com.crone.mvpauth.data.DataManager;
import com.crone.mvpauth.di.modules.LocalModule;
import com.crone.mvpauth.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}