package com.crone.mvpauth.di.components;

import com.crone.mvpauth.di.modules.PicassoCacheModule;
import com.crone.mvpauth.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}