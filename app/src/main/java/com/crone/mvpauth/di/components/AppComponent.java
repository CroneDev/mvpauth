package com.crone.mvpauth.di.components;

import android.content.Context;

import com.crone.mvpauth.di.modules.AppModule;

import dagger.Component;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
