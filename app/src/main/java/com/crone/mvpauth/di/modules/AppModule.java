package com.crone.mvpauth.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by CRN_soft on 12.11.2016.
 */

@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext(){
        return mContext;
    }
}
