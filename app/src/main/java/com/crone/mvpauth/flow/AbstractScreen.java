package com.crone.mvpauth.flow;

import com.crone.mvpauth.mortar.ScreenScoper;

import flow.ClassKey;

/**
 * Created by CRN_soft on 30.11.2016.
 */

public abstract class AbstractScreen<T> extends ClassKey {

    public String getScopeName(){
        return getClass().getName();
    }
    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope(){
        ScreenScoper.destroyScreenScope(getScopeName());
    }


}
