package com.crone.mvpauth.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/**
 * Created by CRN_soft on 21.10.2016.
 */

public class SavedState extends View.BaseSavedState {

    private int state;
    private int load;

    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
        public SavedState createFromParcel(Parcel in) {
            return new SavedState(in);
        }

        public SavedState[] newArray(int size) {
            return new SavedState[size];
        }
    };

    public SavedState(Parcelable superState) {
        super(superState);
    }

    private SavedState(Parcel in) {
        super(in);
        state = in.readInt();
        load = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(state);
        out.writeInt(load);
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int state) {
        this.load = state;
    }
}