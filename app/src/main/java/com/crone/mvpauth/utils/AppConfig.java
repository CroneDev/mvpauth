package com.crone.mvpauth.utils;

/**
 * Created by CRN_soft on 22.10.2016.
 */

public class AppConfig {

    //AuthView, EditText-password minimum characters
    public static final int MINIMUM_CHARACTERS = 8;
    //Backend
    public static final String BASE_URL = "https://www.someurl.com/";
    //Interceptors
    public static long MAX_CONNECTION_TIMEOUT = 5000;
    public static long MAX_READ_TIMEOUT = 5000;
    public static long MAX_WRITE_TIMEOUT = 5000;
    //Fonts path
    public static final String FONTS_BEBAS_NEUE_BOOK = "fonts/PTBebasNeueBook.ttf";
    public static final String FONTS_BEBAS_NEUE_REGULAR = "fonts/PTBebasNeueRegular.ttf";
}
